Time series generation of the format:

```
{
"user_id": 12345,
"heart_rate": 100,
"systolic": 120,
"diastolic": 80
}
```

All values are ints. Values are generated using a normal distribution
around the defaults above given configurable min/max parameters.

Data is generated and written to output in jsonl format (one json
object per line).

= Usage =

```
python setup.py --user install # Install to local user
python -mwfpt.tsgen --help
```
