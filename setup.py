"""setup.py -- setup script

Setuptools config
"""

from setuptools import setup


setup(
    name='wfpt-tsgen',
    version='0.0.0',
    packages=['tsgen'],
    install_requires=[
        'pytest-cov',
        'pytest',
        'coverage',
        'click',
        'scipy'
    ],
    entry_points={
       'console_scripts': [
           'tsgen = tsgen.main:cli'
       ]
    }
)
