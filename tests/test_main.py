"""
test_main.py -- smoke test
"""

import os
import pytest

from click.testing import CliRunner

from tsgen import main


@pytest.fixture(scope='module')
def runner():
    return CliRunner()

def test_main(runner):
    result = runner.invoke(main.cli, ['-s', '10', '-c', '10',
                                      '-o', 'tests/testoutput'])
    assert result.exit_code == 0

    testoutput = open('tests/testoutput', 'rb').read()
    expectedoutput = open('tests/expectedoutput', 'rb').read()
    assert expectedoutput == testoutput
    os.remove('tests/testoutput')
