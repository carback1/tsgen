"""
main.py - main functionality for tsgen tool
"""

import click
import json
import numpy
import sys

from scipy.stats import truncnorm

def get_normal_value_generator(midpoint, minvalue, maxvalue, deviation):
    """Given a midpoint, max, and min, generate a value using a
    gaussian (normal) value with the given standard deviation

    note that numpy's truncnorm is complicated, so this really exists
    to make it human-accessible
    """
    assert minvalue > 0 and maxvalue > 0 and midpoint > 0
    assert minvalue < maxvalue
    assert midpoint > minvalue and midpoint < maxvalue
    return truncnorm((minvalue - midpoint)/deviation,
                     (maxvalue - midpoint)/deviation,
                     loc=midpoint,
                     scale=deviation)

@click.command(help='Generate time series data for programming test')
@click.option('--output', '-o', type=click.File('wb'), default=sys.stdout,
              help='Optional output file')
@click.option('--seed', '-s', default=12345,
              help='Seed to regenerate the data, defaults to 12345')
@click.option('--count', '-c', default=1000000,
              help='Number of values to generate, defaults to 1 million')
def cli(output, seed, count):
    numpy.random.seed(seed=seed)
    generators = {
        "user_id": get_normal_value_generator(50000, 1, 100000, 50000),
        "heart_rate": get_normal_value_generator(50, 1, 200, 10),
        "systolic": get_normal_value_generator(120, 10, 300, 15),
        "diastolic": get_normal_value_generator(80, 1, 200, 15)
    }
    for i in range(count):
        entry = json.dumps({ k: int(v.rvs())
                             for k, v in generators.iteritems() })
        output.write('{}\n'.format(entry))

if __name__ == '__main__':
    cli()
